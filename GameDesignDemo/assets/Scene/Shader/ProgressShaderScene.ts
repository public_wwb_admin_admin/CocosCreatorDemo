// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    ndRich: cc.Node = null;

    @property(cc.Node)
    ndStamp: cc.Node = null;

    _progress: number = 0;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start() {

        let progress = 0;
        let sprRich = this.ndRich.getComponent(cc.Sprite);
        let materials = sprRich.getMaterials();
        sprRich.setMaterial(0, materials[1]);

    }

    update(dt) {
        if (this._progress < 1) {
            this._progress += 0.01 * dt;
            if (this._progress >= 1) {
                this._progress = 1;
            }
        }
    }

    // update (dt) {}
}
