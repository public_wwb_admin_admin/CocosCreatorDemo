

const { ccclass, property } = cc._decorator;

@ccclass
export default class MotionStreakScene extends cc.Component {

    @property(cc.Node)
    mainNode: cc.Node = null;


    // onLoad () {}

    start() {
        let x = this.mainNode.x;
        let y = this.mainNode.y;
        this.mainNode.runAction(
            cc.repeatForever(cc.sequence(cc.moveTo(1.5, x, y + 800),
                cc.moveTo(1.5, x, y)))
        );
    }

    // update (dt) {}
}
