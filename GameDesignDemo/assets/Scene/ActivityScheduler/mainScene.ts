// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import ActivityScheduler from "./ActivityScheduler";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ActivityScene extends cc.Component {


    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start() {
        let newNode = new cc.Node();
        newNode.active = true;
        newNode.parent = this.node;
        let as = newNode.addComponent(ActivityScheduler);

        let now = Date.now();
        let end = now + 1000 * 30;
        as.setAlarmClock(end, () => {
            cc.log("发送活动请求,看看活动结束了没");
        })
    }

    // update (dt) {}
}
